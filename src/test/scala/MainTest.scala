package konrad.tercjak.poker.tests
//import konrad.tercjak.poker.main.Main.randCard
import konrad.tercjak.poker.models.{Card, PlayerModel, Ranks, Suits}
import utest._

object MainTests extends TestSuite {

  val tests = Tests {

    'Exclusive_HandCards_Range - {
      var playerModel = new PlayerModel

      var generator = new HandGen((12 to 0 by -1).toIterator, (0 to 12).toIterator)
      for (i <- 0 to 5) {
        var (card1, card2) = generator.nextHand()
        playerModel.addCards(card1, card2)
      }
      playerModel.normalize()

//      val suited=playerModel.showSuited
//      val unsuited=playerModel.showUnSuited
      val minPair=playerModel.showPair
//      assert(suited=="???????765432")
//      assert(unsuited=="?????????????")
      assert(minPair=="??")

    }

        'Inclusive_HandCards_Range - {
          var playerModel = new PlayerModel
          var generator=new HandGen((12 to 1 by -1).toIterator, (11 to 1 by -1).toIterator)
          for (i <- 0 to 5) {
            var (card1, card2) = generator.nextHand()
            playerModel.addCards(card1, card2)
          }
          playerModel.normalize()

//          val suited=playerModel.showSuited
//          val unsuited=playerModel.showUnSuited
          val minPair=playerModel.showPair


//          assert(  suited=="???????888888")
//          assert(unsuited=="?????????????")
          assert(minPair=="??")
        }

        'Random_HandCards_With32 - {

          var playerModel = new PlayerModel
          var three=Card(Ranks.THREE,Suits.DIAMONDS)
          var two=Card(Ranks.TWO,Suits.DIAMONDS)

          playerModel.addCards(three,two)
          for (i <- 0 to 50) {
            var (card1,card2)=randHand()
            playerModel.addCards(card1, card2)
          }

          playerModel.normalize()

//          val suited=playerModel.showSuited
//          val unsuited=playerModel.showUnSuited
          val minPair=playerModel.showPair

//          assert(suited=="?222222222222")
//          assert(unsuited=="?????????????")
          assert(minPair=="??")


        }
        'Random_HandCards_WithA2 - {
          var playerModel = new PlayerModel
          var ace=Card(Ranks.A,Suits.DIAMONDS)
          var two=Card(Ranks.TWO,Suits.DIAMONDS)

          playerModel.addCards(ace,two)
          for (i <- 0 to 50) {
           var (card1,card2)=randHand()
           playerModel.addCards(card1, card2)
          }

          playerModel.normalize()

//          val suited=playerModel.showSuited
//          val unsuited=playerModel.showUnSuited
          val minPair=playerModel.showPair

//          assert(suited.last=='2')

        }

    class HandGen(rankIter: Iterator[Int], rankIter2: Iterator[Int]) {
      def nextHand(): (Card, Card) = {
        val suit = Suits.CLUBS
        var rank = Ranks(rankIter.next())
        val rank2 = Ranks(rankIter2.next())
        (Card(rank, suit), Card(rank2, suit))
      }

    }

    def randCard(): Card = {
      var rank = Ranks(scala.util.Random.nextInt(13))
      val suit = Suits.CLUBS
      Card(rank, suit)
    }

    def randHand(): (Card,Card) ={
      (randCard ,randCard)
    }



  }
}



