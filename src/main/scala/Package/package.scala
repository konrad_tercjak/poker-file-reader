package konrad.tercjak.poker.filereader

//import konrad.tercjak.poker.filereader.PokerTypes.PlayerHash

//import konrad.tercjak.poker.filereader.TableMap._
import net.jcazevedo.moultingyaml.DefaultYamlProtocol

import scala.beans.BeanProperty

//import scala.beans.BeanProperty
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import scala.collection.parallel.immutable

 case class pokerTableRow(@BeanProperty var id:Int,
                          @BeanProperty var name:String,
                          @BeanProperty var stakes:Float,
                          @BeanProperty var maxSeats:Int){
  override def toString: String = s"#$id '$name' Stake: $stakes $$, Max seats: $maxSeats"
}

object TablesBuilder{
  val tableData= new HashMap[String, (Int,Float)]()
  val seats =new ListBuffer[Int]()

  var nextID: Int = 0
  var lastID: Int = -1

  def getID(key: String, stakes: String): Int = tableData.get(key) match {
      case Some(value) =>
        lastID = value._1
        assert(value._2==stakes.toFloat)
        lastID
      case None =>
        tableData.put(key, (nextID, stakes.toFloat))
        nextID += 1
        lastID += 1
        lastID
    }


  def getID(playerHash:String): Int = {
    tableData(playerHash)._1
  }

  def setNumOfSits(newNumSeats: Int): Unit = {

    if(seats.length>lastID){
      val currNumSeats:Int = seats(lastID)
      seats(lastID) = Math.max(newNumSeats, currNumSeats)
    }
    else {
      seats.append(newNumSeats)
    }
  }

  def toArray(): Array[pokerTableRow] = {
    var y: Array[pokerTableRow] =  (for {
      (key, value) <- tableData
      numOfSits = seats(value._1)
    } yield  pokerTableRow(value._1, key, value._2, numOfSits))(collection.breakOut)

    y.sortBy(x=>x.id)

  }

}

object PlayersStats {
  case class Stats(hash:String,pocketCards:Int,flop:Int)
  var statistics=new ListBuffer[Stats]()
  var counter:Int=0;
  var playersHashes: Map[String, Int] = Map.empty

  //seen Stats

  var pocketCards=new ListBuffer[Int]()
  var flop=new ListBuffer[Int]()
  var turn=new ListBuffer[Int]()
  var river=new ListBuffer[Int]()
  var showdown=new ListBuffer[Int]()

  var pocketCardsCallOrRaise=new ListBuffer[Int]()


  // Call,Fold,Raise,NotCall,NotRaise,Win=6

  //TO DO Collect action data
  def getID(playerHash:String): Int ={
       val playerID= playersHashes.get(playerHash) match {
          case Some(id) =>id
          case None => {
            playersHashes.+=((playerHash,counter));
            initListValues
            counter+=1
            counter-1
          }
        }
    playerID
  }
  def printPocket(): Unit ={

    val reverseMap = for ((k,v) <- playersHashes) yield (v, k)
    var myCounter=0
    var sum=0.0

    var myCounter10=0
    var sum10=0.0

    for (i<-0 to counter-1) {
      if (pocketCards(i) > 5) {
        myCounter += 1
        var playersHash = reverseMap(i)
        var percent: Double = flop(i).toDouble / pocketCards(i)
        sum += percent
        //        println(i + ": " + playersHash + " -> " + percent + "(" + pocketCards(i) + ")")
        if (pocketCards(i) > 10) {
          //          println(i + ": " + playersHash + " -> " + percent + "(" + pocketCards(i) + ")")
          sum10 += percent
          myCounter10 += 1

        }
      }
    }
    println(counter+" Players")

    println("5: "+"avg"+sum/myCounter+"("+myCounter+")")
    println("10: "+"avg"+sum10/myCounter10+"("+myCounter10+")")



      //    pocketCards.foreach(println(x))
  }

  def serialize(): Unit ={

    val reverseMap = for ((k,v) <- playersHashes) yield (v, k)
    var myCounter=0
    var sum=0.0


    for (i<-0 to counter-1) {
      if (pocketCards(i) > 5) {
        myCounter += 1

        var playersHash = reverseMap(i)
        var percent: Double = flop(i).toDouble / pocketCards(i)
        sum += percent
        statistics+=(Stats(playersHash,pocketCards(i),flop(i)))
        //        println(i + ": " + playersHash + " -> " + percent + "(" + pocketCards(i) + ")")

      }
    }
    import com.sksamuel.avro4s.AvroSchema
    val schema = AvroSchema[Stats]

    import java.io.File
    import com.sksamuel.avro4s.AvroOutputStream


    val os = AvroOutputStream.data[Stats](new File("statistics.avro"))
    os.write(statistics)
    os.flush()
    os.close()





    //    pocketCards.foreach(println(x))
  }

  private def initListValues: Unit ={
    pocketCards.append(0)
    flop.append(0)
    turn.append(0)
    river.append(0)
    showdown.append(0)

  }

  private def addPlayerTurns(turnID:Int)(playerHash: String): Unit ={
    var id=getID(playerHash)

    turnID match {
      case 1 => pocketCards(id)+=1
      case 2 => flop(id)+=1
      case 3 => turn(id)+=1
      case 4 => river(id)+=1
      case _ =>println("wrong turn "+turnID)
//      case 4 => flop(id)+=1
    }
//    if(turnID>1){
//      flop(id)+=1
//      if(turnID>2){
//        turn(id)+=1
//        if(turnID>3){
//          river(id)+=1
//        }
//      }
//    }
  }

   def addPlayersTurns(playerHashes: ListBuffer[String],turnID:Int): Unit ={
    val addPlayer:String=>Unit=addPlayerTurns(turnID)(_)
      playerHashes.foreach(x=>addPlayer(x))
  }

  //action(idACtion)(idPlayer)
//  def addStats(playerHash: String,playerHistory:Array[PokerAction.type]): Unit ={
//   var playerID= playersHashes.get(playerHash) match {
//      case Some(id) =>id
//      case None => playersHashes.+=((playerHash,counter))
//    }
//
//
//  }
//  def addShowdownWin(playerHash: String): Unit ={
//
//  }
//  def addShowdownLose(playerHash: String): Unit ={
//
//  }
//    var size=playerHistory.size
//    var action=playerHistory(0)
//    action match {
//      case PokerAction.Call =>
//      case PokerAction.Fold =>
//      case PokerAction.Raise =>
//      case PokerAction.TimeoutCall =>
//      case PokerAction.TimeoutRaise =>
//
//    }
//
//    if(size>1){
//
//    }

//  }
//  def addShowdownWin(playerHash: String): Unit ={
//
//  }
//  def addShowdownLose(playerHash: String): Unit ={
//
//  }

}