package konrad.tercjak.poker.filereader

import java.io.{ByteArrayOutputStream, File, FileOutputStream}
import java.util

import com.sksamuel.avro4s.{AvroDataInputStream, SchemaFor, AvroInputStream => InputStream, AvroOutputStream => OutputStream}
import Abstract.LinesParser
import Extractors.SeatsParser
import FileOps.AvroSaver
import Rows.{ActionInfoRow, SeatsRow, StageRow, TurnHands}
import konrad.tercjak.poker.models._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.Source
object PlayerModels extends mutable.HashMap[String,PlayerModel]{
  def  add(key:String,cards:Array[Card]): Unit ={
    get(key) match {
      case Some(model) =>model.addCards(cards(0),cards(1))
      case None => var model=new PlayerModel
        model.addCards(cards(0),cards(1))
        put(key,model)
    }

  }
}
  class LinesReader(dirName:String) extends LinesParser with TableParser with StageParser
                                      with ShowdownParser with SeatsParser{
    override var lines =Iterator("").buffered

    def recursiveListFiles(dirName: String): Array[File] = {
      new java.io.File(dirName).listFiles.filter(_.getName.endsWith(".txt"))
    }

    var stages = new ListBuffer[StageRow]()
    var seats = new ListBuffer[SeatsRow]()
    var showdowns = new ListBuffer[TurnHands]()

    var actions = new util.TreeMap[Long,List[List[ActionInfoRow]]]()

    var idFile = 0

//    val files = dirName.list.filter(_.getName.endsWith(".txt"))

    for (file <- recursiveListFiles(dirName)) {
      stages = new ListBuffer[StageRow]()
      seats = new ListBuffer[SeatsRow]()

      showdowns = new ListBuffer[TurnHands]()
      actions = new util.TreeMap[Long,List[List[ActionInfoRow]]]()

      lines= Source.fromFile(file).getLines().buffered
      idFile += 1
      Timer.time(readLines())


      assert(actions.size==1000)
      assert(stages.size==1000)
      assert(seats.size==1000)

      YamlSaver.saveStages(stages ,idFile)
      YamlSaver.saveSits(seats,idFile)
      YamlSaver.saveShowdowns(showdowns,idFile)

//      YamlSaver.saveActions(actions,idFile)

      AvroSaver.save(stages.toList,
          OutputStream.data[StageRow](
            new File(s"output/stages/$idFile.avro")
          )
      )

      AvroSaver.save(seats.toList,
        OutputStream.data[SeatsRow](
          new File(s"output/seats/$idFile.avro")
        )
      )
//      implicit val schemaFor = SchemaFor[PlayerHand]

      val fos=new FileOutputStream(s"output/showdowns/$idFile.avro")
      val baos = new ByteArrayOutputStream


      AvroSaver.save(showdowns.toList,
        OutputStream.binary[TurnHands](
          baos
         // new File(s"output/showdowns/$idFile.avro")
        )
      )
      baos.writeTo(fos)

      var readed:AvroDataInputStream[StageRow]=InputStream.data[StageRow](s"output/stages/$idFile.avro")

      assert(readed.iterator().size == 1000)

      import com.sksamuel.avro4s.AvroSchema
//      val schema = AvroSchema[ List[(String, Array[Card])]]
//      println(schema.toString(true))


      println("allShowdown.length : "+showdowns.size)


    }




   var tables= TablesBuilder.toArray()
    YamlSaver.saveTable(tables)

    println("Tables : "+tables.size)

//   tables.foreach(t=>println(t.toString()))

    //    PlayerModel.foreach(x=>{
//      println("\nPlayer: "+x._1+"\n")
//      x._2.normalizeAll()
//      //       println(x._2.unSuited.mkString)
//    })

//   var once= allShowdown.filter(x=>x.exists(y=>y._1=="FEENmRh7HO+E/mtHbgSWhg"))
//    once.foreach(x=>x.foreach(
//     y=> {
//       println(y._1)
//       println(y._2(0).toString() + y._2(1).toString())
//     }
//    ))


//    z.foreach(=>println(id+" "+ cards))

    def readLines():Unit = for {

      line <- lines
      stage <- getStageRow(line)

      idStage = stage.idStage
      _ = stages.prepend(stage)

      (seatsWithCoins, seatMap) <- getSeats(idStage)
      _ = seats.prepend(seatsWithCoins)

      stageActions <- ActionExtractor.get(lines, seatMap,idStage)
      _ = actions.put(idStage,stageActions)

      showdown <- ShowdownExtractor.getCards(lines,idStage,seatMap)
//     _= showdown.foreach(x=>
//       println(x._1+": "+x._2(0).toString()+x._2(1).toString())
//
//     )

     _= showdowns.prepend(showdown)
//      _ = showdown.foreach{ player =>
//        PlayerModels.add(player., player._2)
//      }

      boardData = SummaryExtractor.getBoard(lines)

      boardCards<-boardData
      table=getTable()
//      _=allTables.append()
    }()


  }

