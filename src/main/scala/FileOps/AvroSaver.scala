package FileOps

import java.io.File

import Rows.ActionInfoRow
import com.sksamuel.avro4s.{AvroBinaryOutputStream=>BinaryStream, AvroDataOutputStream => DataStream, AvroOutputStream => OutputStream}

object AvroSaver {
  def save[T](cards: T, os: DataStream[T]): Unit = {
    os.write(cards)
    os.flush()
    os.close()
  }


  def save[T](xs:List[T],os:DataStream[T]):Unit={

    os.write(xs)
    os.flush()
    os.close()
  }
  def save[T](xs:List[T],os:BinaryStream[T]):Unit={

    os.write(xs)
    os.flush()
    os.close()
  }


}
