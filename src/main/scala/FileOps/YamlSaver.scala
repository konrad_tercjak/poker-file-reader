package konrad.tercjak.poker.filereader
import net.jcazevedo.moultingyaml._
import net.jcazevedo.moultingyaml.DefaultYamlProtocol.{yamlFormat1, yamlFormat4, _}
import java.io.PrintWriter
import java.util

import Rows._
import org.yaml.snakeyaml.nodes.Tag
import org.yaml.snakeyaml.representer.Representer
import org.yaml.snakeyaml.{DumperOptions, Yaml}

import scala.beans.BeanProperty
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import scala.collection.{breakOut, mutable}
/**
  * Created by konrad on 18.07.17.
  */
object YamlSaver {


  val options = new DumperOptions
  options.setWidth(1000)
  options.setPrettyFlow(true)
  var yaml = new Yaml(options)

  //    options.setAllowUnicode(false)


  def saveSits(sitsMap:util.TreeMap[Long,util.TreeMap[Int,String]]): Unit ={
    val output : String = yaml.dump(sitsMap)
//    val yaml22 = Map.empty ++ sitsMap.asScala.map(x=>(x._1,Map.empty ++ x._2.asScala))
//    val yaml2=yaml22.toYaml.prettyPrint
//    new PrintWriter("sitsA.yaml") {
//      write(yaml2)
//      close
//    }

    new PrintWriter("sits.yaml") {
      write(output)
      close
    }
  }
  def saveSits(sitsMap:ListBuffer[SeatsRow], fileID:Int): Unit ={ //sitsMap in yaml is TreeMap
//    var sitsMAP=sitsMap.asJava
   var sitsMAP=new java.util.TreeMap[Long,java.util.Map[Int,String]]()

    for (stageSeats:SeatsRow<-sitsMap){
     var seats:java.util.Map[Int, String]= (stageSeats.sitsID, stageSeats.playerHash).zipped.toMap.asJava
     var idStage=stageSeats.idStage
          sitsMAP.put(idStage,seats)
    }

    val output : String = yaml.dump(sitsMAP)

//    val yaml22 = Map.empty ++ sitsMAP.asScala.map(x=>(x._1,Map.empty ++ x._2.asScala))
//    val yaml2=yaml22.toYaml.prettyPrint
//
//    new PrintWriter("output/sitsA/"+fileID+".yaml") {
//      write(yaml2)
//      close
//    }


    new PrintWriter("output/sits/"+fileID+".yaml") {
      write(output)
      close
    }
  }

//  def saveTable(tableMap:ListBuffer[TableRowDB],fileID:Int): Unit ={
//
//    var tableMAP=new java.util.TreeMap[Int,TableDB]()// change names
//
//    for (table:TableRowDB<-tableMap){
//      var value=TableDB(table.tableName,table.numOfSits,table.stakes)
//      var key=table.idTable
////      println(key.toString)
//      tableMAP.put(key,value)
//    }
//
//    val output : String = yaml.dump(tableMAP)
//    new PrintWriter("table"+fileID+".yaml") {
//      write(output)
//      close
//    }
//  }


  def saveTable(table:Array[pokerTableRow]): Unit ={

    import org.yaml.snakeyaml.representer.Representer
    val representer = new Representer

    representer.addClassTag(classOf[pokerTableRow], new Tag("!Table"))
    yaml=new Yaml(representer,options)

        val output : String = yaml.dump(table)
//    val output=table.toYaml(tableFormat).prettyPrint
    new PrintWriter("output/table.yaml") {
      write(output)
      close
    }
  }

  def saveStages(stages: ListBuffer[StageRow], idFile: Int) = {
    val representer = new Representer
    representer.addClassTag(classOf[StageRow], new Tag("!Stage"))
    yaml=new Yaml(representer,options)
    val output : String = yaml.dump(stages.asJava)


    new PrintWriter(s"output/stages/$idFile.yaml") {
      write(output)
      close
    }
  }


  def saveShowdowns(showdowns: ListBuffer[TurnHands], idFile: Int) = {
    val representer = new Representer
    representer.addClassTag(classOf[PlayerHand], new Tag("!PlayerHand"))
    representer.addClassTag(classOf[TurnHands], new Tag("!TurnHands"))

    yaml=new Yaml(representer,options)
    val output : String = yaml.dump(showdowns.asJava)


    new PrintWriter(s"output/showdowns/$idFile.yaml") {
      write(output)
      close
    }
  }


  def saveActions(actions: util.TreeMap[Long, List[List[ActionInfoRow]]], idFile: Int) = {
    val representer = new Representer
    representer.addClassTag(classOf[ActionInfoRow], new Tag("!ActionInfoRow"))
    yaml=new Yaml(representer,options)

    case class Actions(@BeanProperty var key:Long,@BeanProperty var turnList:Array[ActionInfoRow])

  var myAction:Array[Actions]= actions.asScala.map(x=>
    Actions(x._1,x._2.apply(0).toArray)

  )(collection.breakOut)

    val output : String = yaml.dump(myAction)

//var sitsMAP=new java.util.TreeMap[Long,java.util.Map[Int,String]]()

//    for (stageSeats:ActionInfoRow<-actions){
//      var seats:java.util.Map[Int, String]= (stageSeats.sitsID, stageSeats.playerHash).zipped.toMap.asJava
//      var idStage=stageSeats.idStage
//      sitsMAP.put(idStage,seats)
//    }


//
    new PrintWriter(s"output/actions/$idFile.yaml") {
      write(output)
      close
    }
  }


  //  def saveActions(actions: util.TreeMap[Long, List2D[ActionInfo]], fileID: Int) = {
//    import scala.collection.JavaConversions._
//    import scala.collection.mutable.ListBuffer
//    var actionMap= new  util.TreeMap[Long,java.util.List[java.util.List[ActionInfo]]]
//  for (a:(Long, List2D[ActionInfo])<-actions){
//    val v=a._2.map(x=>x.asJava).asJava
////    println(a._2.map(x=>x.map(y=>y.sitID+":"+y.action)+","))
//    actionMap.put(a._1,v)
//   }
//
//
//
//
//    val output : String = yaml.dump(actionMap)
//    new PrintWriter("actions"+fileID+".yaml") {
//      write(output)
//      close
//    }
//  }
}
