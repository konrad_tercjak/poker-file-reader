//package konrad.tercjak.poker.filereader
//
//import java.util
//import java.util.{ArrayList, TreeMap}
//
////import Rows.{StageDB, TableDB}
//
//import konrad.tercjak.poker.models.AllSits
//
//import collection.JavaConversions._
//import konrad.tercjak.poker.filereader.DbOps.DecisionTableRow
//
//import doobie._, doobie.implicits._
//import doobie.postgres._, doobie.postgres.implicits._
//import cats._, cats.data._, cats.effect.IO, cats.implicits._
//
//import scala.collection.mutable.ListBuffer
//
///**
//  * Created by konrad on 18.07.17.
//  */
//object DbOps {
//  var tableMap=new TreeMap[Long,Array[String]]()
//
//  type DecisionTableRow=(Long, Array[Int], Array[Int],Array[Float])
//  type ShutdownTableRow=(Long, Array[Int], Array[Int],Array[Int],Array[Int])
//  type StageTableRow=(Long,Int,Int,String)
//  type  SitsTableRow=(Long,Array[String],Array[Int])
//  type PokerTablesRow=(Int,String,Int,Float)
//  val xa =  Transactor.fromDriverManager[IO](
//    "org.postgresql.Driver", "jdbc:postgresql:pokerdb", "postgres", ""
//  )
//  val y = xa.yolo; import y._
//
//  def insertPocket(idStage:Long, sitsId:Array[Int], playersActions:Array[Int], betValues:Array[Float]):Update0 = {
//
////    implicit val MyJavaEnumAtom = pgJavaEnum[PokerAction]("PokerAction]")
////    import MyJavaEnumAtom._
//
//    sql"insert into POCKETCARDS (idStage,sitsId,playersActions,betValues) values ($idStage, $sitsId,$playersActions,$betValues)".update
//  }
//  def insertPocket(item: (Long, Array[Int], Array[Int], Array[Float])): Update0 ={
//    insertPocket(item._1,item._2,item._3,item._4)
//  }
//
//
//  def insertFlop(idStage:Long, sitsId:Array[Int],playersActions:Array[Int],betValues:Array[Float]):Update0 = {
//
//    //    implicit val MyJavaEnumAtom = pgJavaEnum[PokerAction]("PokerAction]")
//    //    import MyJavaEnumAtom._
//
//    sql"insert into FLOP (idStage,sitsId,playersActions,betValues) values ($idStage, $sitsId,$playersActions,$betValues)".update
//  }
//  def insertFlop(item: (Long, Array[Int], Array[Int], Array[Float])): Update0 ={
//    insertFlop(item._1,item._2,item._3,item._4)
//  }
//
//  def insertTurn(idStage:Long, sitsId:Array[Int],playersActions:Array[Int],betValues:Array[Float]):Update0 = {
//
//    //    implicit val MyJavaEnumAtom = pgJavaEnum[PokerAction]("PokerAction]")
//    //    import MyJavaEnumAtom._
//
//    sql"insert into TURN (idStage,sitsId,playersActions,betValues) values ($idStage, $sitsId,$playersActions,$betValues)".update
//  }
//  def insertTurn(item: (Long, Array[Int], Array[Int], Array[Float])): Update0 ={
//    insertTurn(item._1,item._2,item._3,item._4)
//  }
//
//  def insertRiver(idStage:Long, sitsId:Array[Int],playersActions:Array[Int],betValues:Array[Float]):Update0 = {
//
//    //    implicit val MyJavaEnumAtom = pgJavaEnum[PokerAction]("PokerAction]")
//    //    import MyJavaEnumAtom._
//
//    sql"insert into RIVER (idStage,sitsId,playersActions,betValues) values ($idStage, $sitsId,$playersActions,$betValues)".update
//  }
//  def insertRiver(item: (Long, Array[Int], Array[Int], Array[Float])): Update0 ={
//    insertRiver(item._1,item._2,item._3,item._4)
//  }
//  def insertSits(idStage:Long, playersHashes:Array[String],sitsId:Array[Int]):Update0 = {
//    sql"insert into ALLSITS (idStage, playersHashes,sitsId) values ($idStage, $playersHashes,$sitsId)".update
//  }
//  def insertManySits(ps: List[SitsTableRow]): ConnectionIO[Int] = {
//    val sql ="insert into ALLSITS (idStage, playersHashes,sitsId) values (?, ?,?)"
//
//    Update[SitsTableRow](sql).updateMany(ps)
//  }
//
//  def insertStage(idStage:Long, idTable:Int,dealerID:Int,time:String):Update0 = {
//    sql"insert into STAGES (idStage,idTable,dealerID,time) values ($idStage, $idTable,$dealerID,$time)".update
//  }
//
//  def insertManyStage(ps: List[StageTableRow]): ConnectionIO[Int] = {
//    val sql ="insert into STAGES (idStage,idTable,dealerID,time) values (?, ?,?,?)"
//
//    Update[StageTableRow](sql).updateMany(ps)
//  }
//  def insertTable(idTable:Int, tableName:String, numOfSits:Int,stakes:Float):Update0 = {
//    sql"insert into TABLES (idTable, tableName, numOfSits,stakes) values ($idTable,$tableName,$numOfSits,$stakes)".update
//  }
//  def insertManyTable(ps: List[PokerTablesRow]): ConnectionIO[Int] = {
//    val sql ="insert into TABLES (idTable, tableName, numOfSits,stakes) values (?, ?,?,?)"
//
//    Update[PokerTablesRow](sql).updateMany(ps)//[DecisionTableRow]("id", "name", "age")(ps)
//  }
//
//  def insertManyPocket(ps: List[DecisionTableRow]): ConnectionIO[Int] = {
//    val sql ="insert into POCKETCARDS (idStage,sitsId,playersActions,betValues) values (?, ?,?,?)"
//
//    Update[DecisionTableRow](sql).updateMany(ps)
//  }
//  def insertManyFlop(ps: List[DecisionTableRow]): ConnectionIO[Int] = {
//    val sql ="insert into FLOP (idStage,sitsId,playersActions,betValues) values (?, ?,?,?)"
//
//    Update[DecisionTableRow](sql).updateMany(ps)
//  }
//  def insertManyTurn(ps: List[DecisionTableRow]): ConnectionIO[Int] = {
//    val sql ="insert into TURN (idStage,sitsId,playersActions,betValues) values (?, ?,?,?)"
//
//    Update[DecisionTableRow](sql).updateMany(ps)//[DecisionTableRow]("id", "name", "age")(ps)
//  }
//  def insertManyRiver(ps: List[DecisionTableRow]): ConnectionIO[Int] = {
//    val sql ="insert into River (idStage,sitsId,playersActions,betValues) values (?, ?,?,?)"
//
//    Update[DecisionTableRow](sql).updateMany(ps)//[DecisionTableRow]("id", "name", "age")(ps)
//  }
//
////  def insertManyRiver(ps: List[DecisionTableRow]): ConnectionIO[Int] = {
////    val sql ="insert into River (idStage,sitsId,playersActions,betValues) values (?, ?,?,?)"
////
////    Update[DecisionTableRow](sql).updateMany(ps)//[DecisionTableRow]("id", "name", "age")(ps)
////  }
//
////  def saveSits(sitsMap:TreeMap[Long,HashBiMap[Int,String]]): Unit ={
////  var sitsList:ListBuffer[SitsTableRow]=new ListBuffer[SitsTableRow]()
////    for(tableID:Long<-sitsMap.keySet()){
////      val playerMap:HashBiMap[Int,String]=sitsMap(tableID)
////      val playersHashes=new Array[String](playerMap.size())
////      val playersSit= new Array[Int](playerMap.size())
////    var i:Int=0
////      for((sitsID:Int,playerHash:String)<-playerMap){
////        playersHashes(i)=playerHash
////        playersSit(i)=sitsID
////        i=i+1
////
////      }
////      sitsList.append((tableID, playersHashes,playersSit))
//////      insertSits(tableID, playersHashes,playersSit).quick.unsafePerformSync
////
////    }
////    insertManySits(sitsList.toList).quick.unsafePerformSync
////  }
//
////  def saveTable(tableMap: HashBiMap[Int, TableDB]): Unit ={
////    var tableList:ListBuffer[PokerTablesRow]=new ListBuffer[PokerTablesRow]()
////
////    for(i:Int<-0 until tableMap.size){
////      val table:TableDB=tableMap(i)
////      tableList.append((i,table.tableName,table.numOfSits,table.stakes))
////    }
////    insertManyTable(tableList.toList).quick.run
////
////  }
//
////  def saveStage(stageMap:TreeMap[Long,StageDB]): Unit ={
//////    println(stageMap.size())
////var stageList:ListBuffer[StageTableRow]=new ListBuffer[StageTableRow]()
////
////    var keysIter=stageMap.keySet().iterator()
////    for(i:Int<-0 until stageMap.size()){
////    var key:Long=  keysIter.next()
////      var stage:StageDB=stageMap(key)
////
////      stageList.append((key,stage.idTable,stage.dealerId,stage.time))
//////      insertStage(key,stage.idTable,stage.dealerId,stage.time).quick.unsafePerformSync
////    }
////    insertManyStage(stageList.toList).quick.unsafeRunSync()
////
////  }
//  def savePocketCards(pocketCards: TreeMap[Long, ArrayList[(Int,Float, PokerAction)]]): Unit ={
//  var decisionTable:List[DecisionTableRow]=  toPlayerDecisionTable(pocketCards)
//    insertManyPocket(decisionTable).quick.unsafeRunSync()
//
//  }
//
//  def saveFlop(pocketCards: TreeMap[Long, ArrayList[(Int,Float, PokerAction)]]): Unit ={
//    var decisionTable:List[DecisionTableRow]=  toPlayerDecisionTable(pocketCards)
//    insertManyFlop(decisionTable).quick.unsafeRunSync()
//
//  }
//  def saveTurn(pocketCards: TreeMap[Long, ArrayList[(Int,Float, PokerAction)]]): Unit ={
//    var decisionTable:List[DecisionTableRow]=  toPlayerDecisionTable(pocketCards)
//    insertManyTurn(decisionTable).quick.unsafeRunSync()
//
//  }
//  def saveRiver(pocketCards: TreeMap[Long, ArrayList[(Int,Float, PokerAction)]]): Unit ={
//    var decisionTable:List[DecisionTableRow]=  toPlayerDecisionTable(pocketCards)
//    insertManyRiver(decisionTable).quick.unsafeRunSync()
//
//  }
//  def toPlayerDecisionTable(decisionTable: TreeMap[Long, ArrayList[(Int,Float, PokerAction)]]): List[DecisionTableRow] ={
//    var keysIter=decisionTable.keySet().iterator()
//    var output:ListBuffer[DecisionTableRow]=new ListBuffer[DecisionTableRow]()
//    for(i:Int<-0 until decisionTable.size()){
//      var key:Long=  keysIter.next()
//
//      val action: ArrayList[(Int,Float, PokerAction)]=decisionTable(key)
//
//      var sitsId:Array[Int]=  action.map( {
//        case (id,b,c) => (id)
//      }).toArray
//
//      var playersActions:Array[Int]=  action.map( {
//        case (a,b,pokerAction:PokerAction) => (pokerAction.ordinal)
//      }).toArray
//
//      var betsValue:Array[Float]= action.map( {
//        case (a,betsValue:Float,c) => (betsValue)
//      }).toArray
////      val item:DecisionTableRow=(key,sitsId,playersActions,betsValue)
//      output +=((key,sitsId,playersActions,betsValue))
////      insertPocketCards(key,sitsId,playersActions,betsValue).quick.unsafePerformSync
//    }
//    output.toList
//  }
//  def insertManyShutdown(ps: List[ShutdownTableRow]) = {
//    val sql ="insert into SHUTDOWN (idStage,sitsId,card1,card2,boardCards) values (?, ?,?,?,?)"
//
//    Update[ShutdownTableRow](sql).updateMany(ps)
//  }
//  def saveShutdown(ps: List[ShutdownTableRow]): Unit ={
//    insertManyShutdown(ps).quick.unsafeRunSync()
//  }
//
//
//}
