package konrad.tercjak.poker.main

import java.io.File

import FileOps.AvroSaver
import Rows.SeatsRow
import konrad.tercjak.poker.filereader.{CardsEnum, LinesReader}
//import konrad.tercjak.poker.models.{Ranks, Suits}

import scala.beans.BeanProperty
//import konrad.tercjak.poker.models._
import com.sksamuel.avro4s.{AvroDataInputStream, SchemaFor, AvroInputStream => InputStream, AvroOutputStream => OutputStream}

import scala.collection.immutable.BitSet
import scala.util.Random
import enum.old.{Ranks,Suits}
object Main {
  import konrad.tercjak.poker.filereader.Timer.logging
  def main(args: Array[String]) {
    val dir = "input"

  case class Card(@BeanProperty var rank: Ranks.Value,@BeanProperty var suit: Suits.Value)
    case class MCards(arr:Array[Card])

  val cards1:Array[Card]=(for{
       x:Ranks.Value<-Ranks.values
       y:Suits.Value<-Suits.values
      }yield Card(x,y))(collection.breakOut)


    val cards2=MCards(cards1)

//    val ranksString="TWO,THREE,FOUR,FIVE,SIX,SEVEN,EIGHT,NINE,T,J,Q,K,A".split(',')
//    val suitsString="_H,_C,_D,_S".split(',')
//
//    for{
//      rank<-ranksString
//      suit<-suitsString
//
//        _=println(s"$rank$suit,")
//    }()
//
//    val x:List[CardsEnum]=CardsEnum.values().clone().toList

//
//    import com.sksamuel.avro4s.AvroSchema
//    implicit val schemaFor = SchemaFor[CardsEnum]
//    val schema = AvroSchema[CardsEnum]

    AvroSaver.save(cards2,
      OutputStream.data[MCards](
        new File(s"output/cards/cards.avro")
      )
    )
    println("Hellod")

    //    pokerReader.normalize
//    pokerReader.printResults
//    pokerReader.saveResults


  }

//  class HandGen(rankIter: Iterator[Int], rankIter2: Iterator[Int]) {
//    def nextHand(): (Card, Card) = {
//      val suit = Suits.CLUBS
//      var rank = Ranks(rankIter.next())
//      val rank2 = Ranks(rankIter2.next())
//      // Suits(scala.util.Random.nextInt(Suits.maxId))
//      (Card(rank, suit), Card(rank2, suit))
//    }
//
//  }
//
//  def randCard(): Card = {
//    var rank = Ranks(scala.util.Random.nextInt(12)+1)
//    val suit = Suits.CLUBS // Suits(scala.util.Random.nextInt(Suits.maxId))
//    //      while(rank<Ranks.FIVE||rank==Ranks.NONE){
//    //        rank=  Ranks(scala.util.Random.nextInt(13)
//    //      }
//    Card(rank, suit)
//  }

}
