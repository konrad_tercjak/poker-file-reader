//package enum.old
package konrad.tercjak.poker.models

object Ranks extends Enumeration {
  type Ranks=Value

  val TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, T, J, Q, K, A, NONE = Value


    def fromFile(str:String): Ranks= str match{
        case "2"  => TWO
        case "3"  => THREE
        case "4"  => FOUR
        case "5"  => FIVE
        case "6"  => SIX
        case "7"  => SEVEN
        case "8"  => EIGHT
        case "9"  => NINE
        case "10" => T
        case "J" => J
        case "Q" => Q
        case "K" => K
        case "A" => A
   }

//  def withNameExtended(name: String): Ranks.Value = name match {
//    case "A" | "J" | "Q" | "K" ⇒ super.withName(name)
//    case "2"  ⇒ TWO
//    case "3"  ⇒ THREE
//    case "4"  ⇒ FOUR
//    case "5"  ⇒ FIVE
//    case "6"  ⇒ SIX
//    case "7"  ⇒ SEVEN
//    case "8"  ⇒ EIGHT
//    case "9"  ⇒ NINE
//    case "10" ⇒ T
//    case "T"  ⇒ T
////    case default ⇒
////      NONE
//  }
//  override def toString(): Char = value.toString() match {
//    case "A" | "J" | "Q" | "K" ⇒ value.toString().charAt(0)
//    case "TWO"    ⇒ '2'
//    case "THREE"  ⇒ '3'
//    case "FOUR"   ⇒ '4'
//    case "FIVE"   ⇒ '5'
//    case "SIX"    ⇒ '6'
//    case "SEVEN"  ⇒ '7'
//    case "EIGHT"  ⇒ '8'
//    case "NINE"   ⇒ '9'
//    case "TEN"    ⇒ 'T'
//    case default  ⇒ '?'
//  }


}