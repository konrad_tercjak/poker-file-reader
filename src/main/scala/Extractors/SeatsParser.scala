package Extractors

import Abstract.LinesParser
import Rows.SeatsRow
import konrad.tercjak.poker.filereader.TablesBuilder

import scala.collection.mutable.{ArrayBuffer, HashMap}

import fastparse.all._
trait SeatsParser{
  this: LinesParser =>

  val num: P[Int] = P( CharIn('0'to'9').!.map(_.toInt) )

  val sitID : P[Int] = P( "Seat " ~/ num ~/" - ")
  val hash22 = P(AnyChar.rep(exactly=22)).!.map(_.toString)
  val coins  = P(CharsWhileIn("0123456789.")).!.map(_.toDouble)

  val seatParse=P(sitID~hash22~/" ($"~/coins)

  def getSeats( stageID:Long) :Option[(SeatsRow,HashMap[String, Int])]={
////   var playerHash= new ArrayBuffer[String]()
////    var sitsID= new ArrayBuffer[Int]()
////    var line=lines.next
//
  var (sitsID,playerHash,chips)=  whilePrefix("Seat"){line=>

  val  Parsed.Success(seatsDB, _)=seatParse.parse(line)
      seatsDB

//
//    var seatNumber = line.charAt(5).toString.toInt
//    var hash = line.substring(9,31)
//    var chips = line.substring(34,line.indexOf(" in ")).toFloat

//      (seatNumber,hash,chips)
    }.unzip3

   TablesBuilder.setNumOfSits(playerHash.size)


   var seats:Option[(SeatsRow,HashMap[String, Int])] =

     sitsID.size match {

        case 0    =>    println("#NO SEATS")
                        None

        case _=>        val hashes=playerHash.toArray
                        val sitsId=sitsID.toArray
                        val seatsWithCoins=SeatsRow(stageID,hashes,sitsId)

        val seatsMap :HashMap[String, Int]= new HashMap[String, Int]()

        hashes.zip(sitsId).foreach(
            x => {
              seatsMap.put(x._1, x._2)
            })
          Some((seatsWithCoins,seatsMap))
      }
    seats
  }

//  def getSeat(txt: String): (String,Int,Float) = {
//    var seatNumber = txt.charAt(0).toString.toInt
//    var hash = txt.substring(4,26)
//    var chips = txt.substring(29,txt.indexOf(" in ")).toFloat
//
////SeatInfo(
//  (hash,seatNumber,chips)
//  }
}