package konrad.tercjak.poker.filereader

import Abstract.LinesParser

import scala.collection.mutable.ListBuffer
import konrad.tercjak.poker.models.{Card, Ranks, Suits}


trait ShowdownParser {
  this: LinesParser =>

  def getCards(): Option[List[(String, Array[Card])]] = {
    val playersCards= whileNotPrefix("*** SUMMARY ") {
      getPlayerHand
    }.flatten

    playersCards.length match {
        case 0 => None
        case _ =>Some(playersCards)
    }
  }
  @inline def getPlayerHand(line:String):Option[(String, Array[Card])]={
    var cardArray: Array[Card] = new Array[Card](2)

    var playerHash = line.take(22)
    var data = line.drop(23)
//    println("HASH: "+playerHash)
//    println("data: "+data)
//
//    if (line.size < 23) {
//            println( "line=" + line)
//          }

    data.charAt(0) match {
      case '-' =>
        data.charAt(2) match {
          case 'S' =>
            var c = data.substring(data.indexOf('[') + 1, data.indexOf(']')).split(' ')

            var rank: Ranks = Ranks.fromFile(c(1).dropRight(1))
            var suit: Suits = Suits.fromFile(c(1).takeRight(1))//c(0).takeRight(1))

            cardArray(0) = Card(rank, suit)

            rank = Ranks.fromFile(c(1).dropRight(1))//.withNameExtended(c(1).dropRight(1))
            suit = Suits.fromFile(c(1).takeRight(1))


            cardArray(1) = Card(rank, suit)

          case 'D' => // println(data)                  //Doesnt show
          case 'M' => //println(data)                    // Mucks

          case _ => println("Error"+data)
        }
      case 'C' => //println(data)                       //Collects $11.85 from main pot
      case _ => //println(data)

    }

    if(cardArray.length>0) None
    else {
      Some( (playerHash, cardArray))
    }
  }
}