package konrad.tercjak.poker.filereader

import Rows.{PlayerHand, TurnHands}
import konrad.tercjak.poker.models._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer


object ShowdownExtractor{

  def getCards( lines: Iterator[String],stageId:Long, seatMap: mutable.HashMap[String, Int]): Option[TurnHands] ={
    var playersCards=new ListBuffer[PlayerHand]()
//    var stage:Stage=null

    var line:String= lines.next()

    while (!(line.startsWith("*** SUMMARY "))){
      var cardArray: Array[Card] = new Array[Card](2)

      var playerHash=line.take(22)
      var data=line.drop(23)
//      println(playerHash)
      if(line.size<23){
        println(stageId+"line="+line)
      }

      data.charAt(0) match {
        case '-' =>
          data.charAt(2) match {
            case 'S' =>
              var c= data.substring(data.indexOf('[')+1,data.indexOf(']')).split(' ')

              var rank: Ranks = Ranks.fromFile(c(0).dropRight(1))  // Ranks.withNameExtended(c(0).dropRight(1))
              var suit: Suits = Suits.fromFile(c(0).takeRight(1))
//                Suits.withNameExtended()

                cardArray(0) = Card(rank, suit)
//              if(rank==Ranks.NONE||suit==None){
//                println("error handcards: "+cardArray(0).toString())
//              }

               rank= Ranks.fromFile(c(1).dropRight(1))
                 //Ranks.withNameExtended(c(1).dropRight(1))
               suit= Suits.fromFile(c(1).takeRight(1))
//                 withNameExtended(c(1).takeRight(1))

//              if(rank==Ranks.NONE||suit==None){
//                println("error handcards: "+cardArray(0).toString())
//              }

              cardArray(1) = Card(rank, suit)

//              println(cardArray(0).toString()+" "+cardArray(1).toString())
              var idPlayer=seatMap(playerHash).toShort
//                TablesBuilder.getID(playerHash ).toShort
              playersCards.append(PlayerHand(idPlayer,cardArray))
            case 'D' => // println(data)                  //Doesnt show
            case 'M'=> //println(data)                    // Mucks

            case _ => //println(data)
          }
        case 'C' => //println(data)                       //Collects $11.85 from main pot
        case _ => //println(data)
      }
      line= lines.next()

    }
//    playersCards.map(tuple=>println(tuple._1 +": "+tuple._2(0).toString()+ tuple._2(1).toString()))

    playersCards.length match {
      case 0 => None
      case _ =>
        Some(TurnHands(stageId,playersCards.toArray))
    }
  }

}