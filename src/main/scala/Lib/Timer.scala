package konrad.tercjak.poker.filereader

object Timer {
//  def print_result(s: String, ns: Long) = {
//    val formatter = java.text.NumberFormat.getIntegerInstance
//    println("%-16s".format(s) + formatter.format(ns) + "ns")
//  }

  def time[R](block: => R) : R = {
    var t0 = System.nanoTime()
    var result=block
    var t1 = System.nanoTime()

    var d=(t1-t0)/Math.pow(10,9)
    println("Time: "+d+" s")
    result
  }

  def getTime[R](block: =>R):Double={
    var t0 = System.nanoTime()
    var result=block
    var t1 = System.nanoTime()

    var d=(t1-t0)/Math.pow(10,9)
    d
  }

  // def ntimes[R](N:Int)(block: => R): R ={
  //   val timer =time(_)
  //   var result=block

  //   for (i<- 0 to N){
  //     var result=timer(block)
  //   }
  //   //avg,

  //   result
  // }

  def kestrel[A](x: A)(f: A => Unit): A = { f(x); x }

  //A birdL is called a lark if for
//  any birdsx and y the following holds:
// (Lx)y = x(yy)

  def hummingbird[R,A](after:A)(block: => R)(before :A=>Unit):R={
    val x=after
    val result=block
    before(x)
    result
  }

  def logging[A](x: A) = kestrel(x)(println)

  def logging[A](x: A,y:A):Unit = System.out.printf("%s %s\n",x.toString, y.toString)

//  def logging[A](x: A)(y:A) = System.out.printf("%s%s",x, y);
//  def logging[A](fst:String,x: A)(sec:String,y:A) = System.out.printf("%s%s%s%s",fst,x,sec,y);


  def logging[A](s: String, x: A) = kestrel(x){ y => println(s + ": " + y) }


 // List.fill(N)(block)

//   implicit class ntimes(N:Int) {
//    def ntimes[R](block: => R): Array[R] = List.fill(N)(block)
//  }
//  def time[R](block: => R):R=hummingbird(System.nanoTime)(block){ t0=>
//    val t1 = System.nanoTime()
//    val d=(t1-t0)/Math.pow(10,9)
//    println("Time: "+d+" s")
//  }


}
