package Lib

import scala.collection.mutable.HashMap



class Remember[P,R](f:P=>R){
  val functionMap=HashMap[P,R]()

   def run[S,T](params:P):R={
     val value=f(params)
     functionMap.put(params,value)
    value
   }
  def saveAvro():Unit={
  }

}
