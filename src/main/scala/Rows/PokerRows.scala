package Rows

import konrad.tercjak.poker.filereader.PokerAction
import konrad.tercjak.poker.models.{Card}

import scala.beans.BeanProperty


case class StageRow(@BeanProperty var idStage:Long,@BeanProperty var  idTable: Int,@BeanProperty var dealerId: Int,@BeanProperty var time: Long) //date:Date

case class TableRowDB(idTable: Int, tableName: String,  numOfSits: Int,  stakes: Float)

case class SeatsRow(@BeanProperty var idStage    : Long,
                     @BeanProperty var playerHash : Array[String],
                     @BeanProperty var sitsID     : Array[Int]
)

case class ActionInfoRow(@BeanProperty var sitID:Int,
                         @BeanProperty var stake:Float
                         , @BeanProperty var action: PokerAction
                        )
//case class TurnHands(@BeanProperty var data:List[PlayerHand])

case class PlayerHand(@BeanProperty var idPlayer:Short,@BeanProperty var cards: Array[Card])

case class TurnHands( @BeanProperty var idStage:Long, @BeanProperty var history:Array[PlayerHand])



//sealed trait TurnHistory{
//
//}
//
//case class PoketCardsHistory(actions:List[ActionInfo],next:Either[TurnHistory,ShowdownHistory] ) extends TurnHistory
//case class TurnHistory(actions:List[ActionInfo],next:Either[FlopHistory,ShowdownHistory] ) extends TurnHistory
//case class FlopHistory(actions:List[ActionInfo],next:Either[RiverHistory,ShowdownHistory] ) extends TurnHistory
//case class RiverHistory(actions:List[ActionInfo],next:ShowdownHistory ) extends TurnHistory
//case class ShowdownHistory(i: Int) extends TurnHistory {
//
//}
