package konrad.tercjak.poker.models

import java.util
import java.util.Date


import scala.beans.BeanProperty

case class AllSits(idStage:Long,playersHashes:Array[String],sitsId:Array[Int])


case class  Stage(stageID: Long, gameType: String, money: Float, date: String){
 def asString(): String ={
//   Stage #3017235114: Holdem  No Limit $0.50 - 2009-07-01 00:00:01 (ET)
   s"Stage #$stageID: $gameType $$$money - $date"
 }
} //date:Date

case class StageData(@BeanProperty  gameType: String, @BeanProperty  money: Float,
                    @BeanProperty  date: String,@BeanProperty name: String,
                    @BeanProperty  dealerSeat: Short)

case class Table( name: String, dealerSeat: Short){
  def asString(): String ={
    //  Table: DEFIANCE ST (Real Money) Seat #6 is the dealer
    s"Table : $name Seat #$dealerSeat is dealer"
  }
}
case class TableData( name: String, dealerSeat: Short){
  def asString(): String ={
    //  Table: DEFIANCE ST (Real Money) Seat #6 is the dealer
    s"Table : $name Seat #$dealerSeat is dealer"
  }
}


case class TableInfo( stage: Stage, table:Table, _seats: util.ArrayList[SimplePlayerSeat]){
   var seats:util.TreeMap[Int,String]=new util.TreeMap[Int,String]()
  for(i<-0 until _seats.size()){
    seats.put(_seats.get(i).seatNumber,_seats.get(i).playerHash)
  }
}



case class TotalStake(pot: Option[Float],
                     rake: Option[Float],
                     jackpotRake: Option[Float])



case class SimplePlayerSeat(playerHash:String,seatNumber:Int)
case class SeatInfo(playerHash:String,seatNumber:Int,coin:Double){
//  Seat 5 - /vGteJlrArnt5ADwBBNyBA ($16.25 in chips)
def asString(): String ={
  s"Seat $seatNumber - $playerHash ($$$coin in chips)"
}
}



object PlayerAction extends Enumeration {
  val Fold, Call, Raise = Value
}

object PokerTurns extends Enumeration {
  val NONE, POCKET_CARDS, FLOP, RIVER, TURN, SHOW_DOWN, SUMMARY = Value
  def withNameExtended(s:String)={
    withName(s.replace(' ', '_'))
  }
  def getTurnFromFileLine(line: String) = {
    withName(line.substring(1, (line indexOf '*') - 1)
                .replace(' ', '_'))
  }
}

//



object PlayerMark extends Enumeration {
  val small_blind, big_blind, dealer = Value

  def withNameExtended(name: String): PlayerMark.Value = name match {
    case "small blind"  ⇒ small_blind
    case "big blind"    ⇒ big_blind
    case "dealer"       ⇒ dealer

  }
}
