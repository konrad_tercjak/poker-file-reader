package konrad.tercjak.poker.filereader

class TuplesFunctions {
  def f2[A,B,C](t: ((A,B),C)) = (t._1._1, t._1._2, t._2)

  def f3[A,B,C,D](t: ((A,B),C,D)) = (t._1._1, t._1._2, t._2, t._3)
}
