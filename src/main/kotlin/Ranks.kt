package konrad.tercjak.poker.models

enum class Ranks(val r:Char){
    TWO("2"),
    THREE("3"),
    FOUR("4"),
    FIVE("5"),
    SIX("6"),
    SEVEN("7"),
    EIGHT("8"),
    NINE("9"),
    T("T"),
    J("J"),
    Q("Q"),
    K("K"),
    A("A"),
    NONE("-");
    override override fun toString(): String {
        return r.toString()
    }
  fun fromFile(str:String): Unit {
         when(str){
                  "A" | "J" | "Q" | "K"|"T" -> withName(name)
                 case "2"  -> TWO
                 case "3"  -> THREE
                 case "4"  -> FOUR
                 case "5"  -> FIVE
                 case "6"  -> SIX
                 case "7"  -> SEVEN
                 case "8"  -> EIGHT
                 case "9"  -> NINE
                 case "10" -> T
      }

        }

}