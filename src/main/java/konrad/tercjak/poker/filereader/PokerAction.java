/**
 * Created by konrad on 10.07.17.
 */
package konrad.tercjak.poker.filereader;

public  enum PokerAction {
    Fold("F"),
    Call("C"),
    Checks("c"),
    Raise("R"),
    Bets("B"),
    TimeoutChecks("tc"),
    TimeoutFold("tF"),
    Return("Re")
    ;

    String shortName;

    PokerAction(String c) {
        shortName = c;
    }
}