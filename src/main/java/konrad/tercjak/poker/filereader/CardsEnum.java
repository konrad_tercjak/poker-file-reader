package konrad.tercjak.poker.filereader;

import java.io.Serializable;

public enum CardsEnum  implements Serializable {
    TWO_H,
    TWO_C,
    TWO_D,
    TWO_S,
    THREE_H,
    THREE_C,
    THREE_D,
    THREE_S,
    FOUR_H,
    FOUR_C,
    FOUR_D,
    FOUR_S,
    FIVE_H,
    FIVE_C,
    FIVE_D,
    FIVE_S,
    SIX_H,
    SIX_C,
    SIX_D,
    SIX_S,
    SEVEN_H,
    SEVEN_C,
    SEVEN_D,
    SEVEN_S,
    EIGHT_H,
    EIGHT_C,
    EIGHT_D,
    EIGHT_S,
    NINE_H,
    NINE_C,
    NINE_D,
    NINE_S,
    T_H,
    T_C,
    T_D,
    T_S,
    J_H,
    J_C,
    J_D,
    J_S,
    Q_H,
    Q_C,
    Q_D,
    Q_S,
    K_H,
    K_C,
    K_D,
    K_S,
    A_H,
    A_C,
    A_D,
    A_S;
}
