name := "Poker File Reader"

organization := "konrad.tercjak"

version := "0.0.1"

fork := true
testFrameworks += new TestFramework("utest.runner.Framework")
//scalaVersion := "2.11.8"
scalaVersion := "2.12.4"

//lazy val doobieVersion = "0.4.4"
lazy val doobieVersion = "0.5.0-M10"  //"0.5.0-M9"
val CirceVersion = "0.8"
//val CatsVersion = "0.9"

//val circeVersion = "0.9.0-M2"
resolvers ++= Seq(
  "softprops-maven" at "http://dl.bintray.com/content/softprops/maven",
  "Sonatype OSS Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/",
  "Atlassian Releases" at "https://maven.atlassian.com/public/",
  "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/",
  "Central" at "http://central.maven.org/maven2/",
  "central" at "http://repo1.maven.org/maven2",
  "Spring" at "http://repo.spring.io/plugins-release/",
  "Sun Maven2 Repo" at "http://download.java.net/maven/2"
)


libraryDependencies ++= Seq(
   "com.lihaoyi" %% "utest" % "0.6.3" % "test",
  "com.lihaoyi" %% "fastparse" % "1.0.0",
//  "org.tpolecat" %% "asbttto-core" % "0.6.1-M6",
//    "io.circe" %% "circe-yaml" % "0.6.1",
  "org.tpolecat" %% "doobie-core"     % doobieVersion,
  "org.tpolecat" %% "doobie-postgres" % doobieVersion,
//  "org.tpolecat" %% "doobie-specs2"   % doobieVersion,
  "com.sksamuel.avro4s" %%  "avro4s-core" % "1.8.0",

  "codes.reactive" %% "scala-time" % "0.4.1",
  "net.jcazevedo" %% "moultingyaml" % "0.4.0",
  "org.yaml" % "snakeyaml" % "1.18"
//  "pl.project13.scala" % "sbt-jmh-extras" % "0.2.27"
  //  "org.scalacheck" %% "scalacheck" % "2.12" % "test" withSources () withJavadoc ()
)

initialCommands := "import konrad.tercjak.poker._"

//testFrameworks += new TestFramework(
//  "org.scalameter.ScalaMeterFramework")

logBuffered := false
parallelExecution in Test := false